function RequestPopupButton() {
    RequestPopupButton.superclass.constructor.apply(this, arguments);
}

extend(RequestPopupButton, PopupButton);
RequestPopupButton.prototype.pattern = '.request-popup';
RequestPopupButton.prototype.callback = function (selector) {
    PopupButton.prototype.callback.apply(this, arguments);
}
core.autoload(RequestPopupButton);