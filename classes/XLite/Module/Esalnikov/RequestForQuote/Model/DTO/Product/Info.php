<?php

namespace XLite\Module\Esalnikov\RequestForQuote\Model\DTO\Product;

/**
 * Abstract widget
 */
abstract class Info extends \XLite\Model\DTO\Product\Info implements \XLite\Base\IDecorator
{
    protected function init($object)
    {
        parent::init($object);

        $this->default->requestForQuote = $object->getRequestForQuote();
    }

    /**
     * @param \XLite\Model\Product $object
     * @param array|null $rawData
     *
     * @return mixed
     */
    public function populateTo($object, $rawData = null)
    {
        parent::populateTo($object, $rawData);

        $object->setRequestForQuote((boolean)$this->default->requestForQuote);
    }
}