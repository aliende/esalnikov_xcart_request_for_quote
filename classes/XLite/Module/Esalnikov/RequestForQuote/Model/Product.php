<?php

namespace XLite\Module\Esalnikov\RequestForQuote\Model;

abstract class Product extends \XLite\Model\Product implements \XLite\Base\IDecorator
{
    /**
     * @Column (type="boolean")
     */
    protected $requestForQuote;

    public function getRequestForQuote()
    {
        return $this->requestForQuote;
    }

    public function setRequestForQuote($value)
    {
        $this->requestForQuote = $value;
        return $this;
    }
}