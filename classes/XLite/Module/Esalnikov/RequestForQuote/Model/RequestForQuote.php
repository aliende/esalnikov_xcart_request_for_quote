<?php

namespace XLite\Module\Esalnikov\RequestForQuote\Model;

class RequestForQuote extends \XLite\Model\Base\NonPersistentEntity
{
    /**
     * @var string
     */
    protected $email;

    /**
     * Set Email
     *
     * @param $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Return Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}