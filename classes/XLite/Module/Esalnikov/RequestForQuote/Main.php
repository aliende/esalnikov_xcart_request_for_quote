<?php

namespace XLite\Module\Esalnikov\RequestForQuote;

use XLite\Module\AModule;

abstract class Main extends AModule
{
    /**
     * Author name
     *
     * @return string
     */
    public static function getAuthorName()
    {
        return 'Eugene Salnikov';
    }

    /**
     * Module name
     *
     * @return string
     */
    public static function getModuleName()
    {
        return 'Request for quote';
    }

    /**
     * Module major version
     *
     * @return string
     */
    public static function getMajorVersion()
    {
        return '5.3';
    }

    /**
     * Module minor version
     *
     * @return int|string
     */
    public static function getMinorVersion()
    {
        return 0;
    }

    /**
     * Module description
     *
     * @return string
     */
    public static function getDescription()
    {
        return 'Module adds checkbox to product in admin panel and replases "Add to cart" button with "Get quote" button. When customer clicks "Get quote" button, a pop-up window is opened. This pop-up window contains "E-mail" text field.         The customer should fill this field and click "Send" button. Admin of the store receives a letter with contact e-mail of a customer with a quote.';
    }


}