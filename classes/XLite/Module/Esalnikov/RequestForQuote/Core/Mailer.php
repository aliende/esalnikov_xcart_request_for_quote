<?php

namespace XLite\Module\Esalnikov\RequestForQuote\Core;

abstract class Mailer extends \XLite\Core\Mailer implements \XLite\Base\IDecorator
{
    public static function sendRequestNotification($customer_email, $product_name, $full_product_link)
    {
        static::register('customer_email', $customer_email);
        static::register('product_name', $product_name);
        static::register('full_product_link', $full_product_link);

        static::compose(
            'RequestForQuote',
            static::getSiteAdministratorMail(),
            static::getSiteAdministratorMail(),
            'modules/Esalnikov/RequestForQuote/message',
            [],
            true,
            \XLITE::ADMIN_INTERFACE
        );

        return static::getMailer()->getLastError();

    }
}