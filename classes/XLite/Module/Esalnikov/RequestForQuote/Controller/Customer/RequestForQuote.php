<?php

namespace XLite\Module\Esalnikov\RequestForQuote\Controller\Customer;

class RequestForQuote extends \XLite\Controller\Customer\ACustomer
{
    public function getTitle()
    {
        return 'Request for quote';
    }

    protected function doActionSubmit()
    {
        $data = \XLite\Core\Request::getInstance()->getData();
        $customer_email = $data['customerEmail'];
        $product_id = $data['product_id'];
        $product = \XLite\Core\Database::getRepo('\XLite\Model\Product')->findOneBy(['product_id' => $product_id]);
        $product_name = $product->name;
        $product_link = $this->buildURL('product', '', ['product_id' => $product_id]);
        $full_product_link = $this->buildFullURL('product', '', ['product_id' => $product_id]);
        \XLite\Core\Mailer::sendRequestNotification($customer_email, $product_name, $full_product_link);
        \XLite\Core\TopMessage::addInfo(static::t('Thank you! Your request has been successfully sent'));
        $this->setReturnURL($product_link);
    }
}