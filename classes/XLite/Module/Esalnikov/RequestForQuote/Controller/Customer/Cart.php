<?php

namespace XLite\Module\Esalnikov\RequestForQuote\Controller\Customer;

abstract class Cart extends \XLite\Controller\Customer\Cart implements \XLite\Base\IDecorator
{
    protected function addItem($item)
    {
        if ($item->getProduct()->getRequestForQuote()) {
            $result = false;
            \XLite\Core\TopMessage::addError('This product cannot be added to cart.');
        } else {
            $result = parent::addItem($item);
        }

        return $result;
    }
}