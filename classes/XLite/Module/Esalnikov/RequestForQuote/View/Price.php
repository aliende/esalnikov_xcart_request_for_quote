<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View;

class Price extends \XLite\View\Price implements \XLite\Base\IDecorator
{
    protected function isVisible()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return parent::isVisible();
    }


}