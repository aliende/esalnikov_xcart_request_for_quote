<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View;

class RequestWidget extends \XLite\View\AView
{
    protected function getDefaultTemplate()
    {
        return 'modules/Esalnikov/RequestForQuote/page/request_widget.twig';
    }

    public static function getAllowedTargets()
    {
        return array_merge(parent::getAllowedTargets(), array('request_for_quote', 'product', 'catalog'));
    }
}