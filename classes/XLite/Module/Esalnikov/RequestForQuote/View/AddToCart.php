<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View;

/**
 * Add to cart widget
 *
 * @Decorator\Depend("XC\ProductComparison")
 */
abstract class AddToCart extends \XLite\Module\XC\ProductComparison\View\AddToCart implements \XLite\Base\IDecorator
{
    /**
     * Check if widget is visible
     *
     * @return boolean
     */
    protected function isVisible()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return parent::isVisible();
    }
}