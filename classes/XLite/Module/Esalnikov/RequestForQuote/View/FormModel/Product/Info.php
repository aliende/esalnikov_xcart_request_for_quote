<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\FormModel\Product;


abstract class Info extends \XLite\View\FormModel\Product\Info implements \XLite\Base\IDecorator
{
    protected function defineFields()
    {
        $schema = parent::defineFields();

        $schema['default']['requestForQuote'] = [
            'label'     => static::t('Request for quote'),
            'type'      => 'XLite\View\FormModel\Type\SwitcherType',
            'position'  => 900,
        ];

        return $schema;
    }

}