<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\Product\Details\Customer;

class AddButton extends \XLite\View\Product\Details\Customer\AddButton implements \XLite\Base\IDecorator
{
    protected function isVisible()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return parent::isVisible();
    }
}