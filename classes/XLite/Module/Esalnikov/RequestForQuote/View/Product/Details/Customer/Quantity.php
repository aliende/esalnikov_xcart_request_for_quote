<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\Product\Details\Customer;

class Quantity extends \XLite\View\Product\Details\Customer\Quantity implements \XLite\Base\IDecorator
{
    protected function isVisible()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return parent::isVisible();
    }
}