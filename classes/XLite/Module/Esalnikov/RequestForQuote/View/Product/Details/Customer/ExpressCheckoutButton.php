<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\Product\Details\Customer;
/**
 * Add to cart widget
 *
 * @Decorator\Depend("CDev\Paypal")
 */
class ExpressCheckoutButton extends \XLite\Module\CDev\Paypal\View\Product\ExpressCheckoutButton implements \XLite\Base\IDecorator
{
    protected function isVisible()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return parent::isVisible();
    }
}