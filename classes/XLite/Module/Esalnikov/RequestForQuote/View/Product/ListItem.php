<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\Product;

abstract class ListItem extends \XLite\View\Product\ListItem implements \XLite\Base\IDecorator
{
    protected function isShowAdd2CartBlock()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return true;
    }

    protected function isDisplayAdd2CartButton()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return true;
    }

    protected function isDisplayGridAdd2CartButton()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return true;
    }

    protected function isQuickLookEnabled()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return false;
        }
        return true;
    }
}