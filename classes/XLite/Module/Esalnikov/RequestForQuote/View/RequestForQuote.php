<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View;

/**
 * @ListChild (list="product.details.page.info", zone="customer", weight="40")
 * @ListChild (list="product.details.quicklook.info", zone="customer", weight="40")
 */

class RequestForQuote extends \XLite\View\AView
{
    protected function getDefaultTemplate()
    {
        return 'modules/Esalnikov/RequestForQuote/product/request_for_quote.twig';
    }

    protected function isVisible()
    {
        if ($this->getProduct()->getRequestForQuote()) {
            return true;
        }
        return false;
    }
}