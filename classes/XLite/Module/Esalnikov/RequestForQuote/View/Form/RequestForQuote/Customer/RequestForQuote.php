<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\Form\RequestForQuote\Customer;

class RequestForQuote extends \XLite\View\Form\AForm
{
    const PARAM_PRODUCT_ID = 'product_id';

    protected function getDefaultTarget()
    {
        return 'request_for_quote';
    }

    protected function getDefaultAction()
    {
        return 'submit';
    }

    protected function getDefaultParams()
    {
        $params = [
            self::PARAM_PRODUCT_ID => \XLite\Core\Request::getInstance()->product_id,
        ];

        return $params;
    }

    


}