<?php

namespace XLite\Module\Esalnikov\RequestForQuote\View\Button;

class RequestPopupButton extends \XLite\View\Button\APopupButton
{
    public function getJSFiles()
    {
        $list = parent::getJSFiles();
        $list[] = 'modules/Esalnikov/RequestForQuote/page/popup_button.js';
        return $list;
    }

    protected function prepareURLParams()
    {
        return array(
            'target' => 'request_for_quote',
            'product_id' => $this->getProduct() ? $this->getProduct()->getProductId() : null,
            'widget' => '\XLite\Module\Esalnikov\RequestForQuote\View\RequestWidget',
        );
    }

    protected function getClass()
    {
        return parent::getClass() . ' request-popup';
    }
}